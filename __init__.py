# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import surveillance
from . import dash


def register():
    Pool.register(
        surveillance.AppSurveillanceSchedule,
        dash.DashApp,
        module='dash_surveillance', type_='model')
